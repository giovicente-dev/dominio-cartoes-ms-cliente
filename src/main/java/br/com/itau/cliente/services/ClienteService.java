package br.com.itau.cliente.services;

import br.com.itau.cliente.exceptions.ClienteNotFoundException;
import br.com.itau.cliente.repositories.ClienteRepository;
import br.com.itau.cliente.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente consultarClientePorId(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()) {
            return clienteOptional.get();
        }

        throw new ClienteNotFoundException();
    }

}
