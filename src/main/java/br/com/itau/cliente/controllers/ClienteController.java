package br.com.itau.cliente.controllers;

import br.com.itau.cliente.dtos.ClienteDTOEntradaPost;
import br.com.itau.cliente.exceptions.ClienteNotFoundException;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> postCliente(@RequestBody ClienteDTOEntradaPost clienteDTOEntradaPost) {
        System.out.println("Fui requisitado para um post! " + System.currentTimeMillis());

        Cliente cliente = new Cliente();
        cliente.setName(clienteDTOEntradaPost.getName());

        Cliente clienteObjeto = clienteService.salvarCliente(cliente);

        return ResponseEntity.status(201).body(clienteObjeto);
    }

    @GetMapping("/{id}")
    public Cliente getClienteById(@PathVariable(name = "id") Long id) {
        System.out.println("Fui requisitado para um get! " + System.currentTimeMillis());

        try {
            Cliente clienteObjeto = clienteService.consultarClientePorId(id);
            return clienteObjeto;
        } catch (ClienteNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente não encontrado.");
        }
    }

}
